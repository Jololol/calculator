﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    

    public partial class CalculatorForm : Form
    {
        double memory = 0;
        double previous = 0;
        double current;

        Operations currentOp;

        enum Operations { Add, Subtract, Multiply, Divide};

        bool operationReady = false;
        bool resetDisplay = false;

        
        

        public CalculatorForm()
        {
            InitializeComponent();
            zeroButton.Focus();
        }

        double performOperation(double a, double b, Operations op)
        {
            if(op == Operations.Add)
            {
                return a + b;
            } else if (op == Operations.Subtract)
            {
                return a - b;
            } else if (op == Operations.Multiply)
            {
                return a * b;
            } else 
            {
                return a / b;
            }
        }

        
        // Adds a number to the current calculator display.
        // If the display is currently 0, it removes 0 before adding number.
        // This helps application act like a standard calculator where typing 0
        // when 0 is screen doesn't change anything.
        private void NumberPress(object sender, EventArgs e)
        {
            
            // Convert button label 0 - 9 to int
            Button button = sender as Button;
            int num = Convert.ToInt16(button.Text);

            if (Convert.ToDouble(calcDisplay.Text) == 0 || resetDisplay == true)
            {
                calcDisplay.Text = Convert.ToString(num);
                resetDisplay = false;
            } else
            {
                calcDisplay.Text = calcDisplay.Text + button.Text;
            }
            
        }



        // Function to clear display to 0 after clicking "C" button
        private void clearButton_Click(object sender, EventArgs e)
        {
            double previous = 0;
            calcDisplay.Text = "0";
        }

       
        // Method for pressing = Button
        private void equalsButton_Click(object sender, EventArgs e)
        {
            current = Convert.ToDouble(calcDisplay.Text);
            current = performOperation(previous, current, currentOp);
            calcDisplay.Text = Convert.ToString(current);
            previous = 0;
            operationReady = false;
            resetDisplay = true;

            // Clear current operation "selection"
            addButton.FlatAppearance.BorderSize = 1;
            minusButton.FlatAppearance.BorderSize = 1;
            multiplyButton.FlatAppearance.BorderSize = 1;
            divideButton.FlatAppearance.BorderSize = 1;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {

            if (calcDisplay.Text.Length == 1)
            {
                calcDisplay.Text = "0";
            }
            else
            {

                string display = calcDisplay.Text;
                calcDisplay.Text = display.Remove(display.Length - 1, 1);
            }
        }

        private void decimalButton_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (calcDisplay.Text.Contains("."))
                 return;
            else 
                calcDisplay.Text = calcDisplay.Text + button.Text;
        }

        private void negativeButton_Click(object sender, EventArgs e)
        {
            double num = Convert.ToDouble(calcDisplay.Text);
            num = num * (-1); // Swap
            calcDisplay.Text = Convert.ToString(num);

        }

        // Method for pressing + Button
        private void addButton_Click(object sender, EventArgs e)
        {
            // Change border to show this operation is selected
            addButton.FlatAppearance.BorderSize = 2;
            minusButton.FlatAppearance.BorderSize = 1;
            multiplyButton.FlatAppearance.BorderSize = 1;
            divideButton.FlatAppearance.BorderSize = 1;
            
            if (operationReady == false)
            {
                currentOp = Operations.Add;
                previous = Convert.ToDouble(calcDisplay.Text);
                resetDisplay = true;
            }
            if (operationReady == true)
            {
                previous += Convert.ToDouble(calcDisplay.Text);
                calcDisplay.Text = Convert.ToString(previous);
            }

        }

        private void minusButton_Click(object sender, EventArgs e)
        {
            addButton.FlatAppearance.BorderSize = 1;
            minusButton.FlatAppearance.BorderSize = 2;
            multiplyButton.FlatAppearance.BorderSize = 1;
            divideButton.FlatAppearance.BorderSize = 1;

            if (operationReady == false)
            {
                currentOp = Operations.Subtract;
                previous = Convert.ToDouble(calcDisplay.Text);
                resetDisplay = true;
            }
            if (operationReady == true)
            {
                previous -= Convert.ToDouble(calcDisplay.Text);
                calcDisplay.Text = Convert.ToString(previous);
            }
        }

        private void multiplyButton_Click(object sender, EventArgs e)
        {
            addButton.FlatAppearance.BorderSize = 1;
            minusButton.FlatAppearance.BorderSize = 1;
            multiplyButton.FlatAppearance.BorderSize = 2;
            divideButton.FlatAppearance.BorderSize = 1;
            
            if (operationReady == false)
            {
                currentOp = Operations.Multiply;
                previous = Convert.ToDouble(calcDisplay.Text);
                resetDisplay = true;
            }
            if (operationReady == true)
            {
                previous *= Convert.ToDouble(calcDisplay.Text);
                calcDisplay.Text = Convert.ToString(previous);
            }
        }

        private void divideButton_Click(object sender, EventArgs e)
        {
            addButton.FlatAppearance.BorderSize = 1;
            minusButton.FlatAppearance.BorderSize = 1;
            multiplyButton.FlatAppearance.BorderSize = 1;
            divideButton.FlatAppearance.BorderSize = 2;

            if (operationReady == false)
            {
                currentOp = Operations.Divide;
                previous = Convert.ToDouble(calcDisplay.Text);
                resetDisplay = true;
            }
            if (operationReady == true)
            {
                previous = previous / Convert.ToDouble(calcDisplay.Text);
                calcDisplay.Text = Convert.ToString(previous);
            }
        }

        private void memoryStoreButton_Click(object sender, EventArgs e)
        {
            memory = Convert.ToDouble(calcDisplay.Text);
            memoryRecallButton.FlatAppearance.BorderSize = 2;
            resetDisplay = true;
            memoryRecallButton.Focus();
        }

        private void memoryRecallButton_Click(object sender, EventArgs e)
        {
            calcDisplay.Text = Convert.ToString(memory); // Pulls current value stored in memory
        }

        private void memoryClearButton_Click(object sender, EventArgs e)
        {
            memory = 0; // Clear memory
            memoryRecallButton.FlatAppearance.BorderSize = 1; // Remove border to indicate no memory stored
        }
    }
}
